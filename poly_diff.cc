#include "poly_diff.h"
#include <string>
#include <stdlib.h>
#include <math.h>

using namespace std;

Polynomial::Polynomial(string Expression){
  string temp = Expression;
  do {
    int coefficient,difference;
    temp = Expression.substr(0, Expression.find('+'));

    if (temp.find('x') != string::npos) {
      if (temp.substr(0, temp.find('x')) == "")  coefficient = 1;
      else if (temp.substr(0, temp.find('x')) == "-")  coefficient = -1;
      else coefficient = atoi((temp.substr(0, temp.find('x'))).c_str());
    }
    else {
      coefficient = atoi(temp.c_str());
    }
    Coefficent_.push_back(coefficient);


    if (temp.find('^') == string::npos && temp.find('x') == string::npos)  difference = 0;
    else if (temp.find('^') == string::npos)  difference = 1;
    else difference = atoi((temp.substr(temp.find('^') + 1, temp.length() - temp.find('^') - 1)).c_str());
    Difference_.push_back(difference);

    if (Expression.find('+') != string::npos)  Expression.erase(0, Expression.find('+') + 1);
    else Expression = "";
  } while (Expression != "");
}

int Polynomial::Differentiation(const int& value){
  int result = 0;
  for (int i=0; i<Coefficent_.size(); ++i) {
    result += Coefficent_[i] * Difference_[i] * pow(value, Difference_[i] - 1);
  }
  return result;
}
