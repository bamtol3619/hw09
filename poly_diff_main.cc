#include "poly_diff.h"
#include <iostream>

using namespace std;

int main(){
  string cmd;
  int value;
  while (true) {
    cin >> cmd;
    if (cmd == "quit")  break;
    Polynomial Poly(cmd);
    cin >> value;
    cout << Poly.Differentiation(value) << endl;
  }
  return 0;
}