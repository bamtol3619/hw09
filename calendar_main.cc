// calendar_main.cc

#include <iostream>
#include <string>

using namespace std;
#include "calendar.h"

int main() {
  Date date; 
  string cmd;
  while (cmd != "quit") {
    cin >> cmd;
    try {
      if (cmd == "set") {
        cin >> date;
        cout << date << endl;
      } else if (cmd == "next") {
        int day = 1;
        cin >> day;
        date.NextDay(day);
        cout << date << endl;
      } else if (cmd == "next_day") {
        date.NextDay(1);
        cout << date << endl;
      }
    } catch (InvalidDateException& e) {
      cout << "Invalid date: " << e.input_date << endl;
    }
  }
  return 0;
}