#include "grader.h"
#include <iostream>

using namespace std;

// 성적을 Pass / Fail 로 구분하여 출력해주는 클래스.
// 성적이 pass_score보다 같거나 높으면 "P", 아니면 "F"를 리턴.

SubjectPassFail::SubjectPassFail(const string& name, int credit, int pass_score) : Subject(name, credit), pass_score_(pass_score) {}

SubjectPassFail::~SubjectPassFail(){}

string SubjectPassFail::GetGrade(int score) const{
  if(score >= pass_score_)  return "P";
  else  return "F";
}

// 성적을 A, B, C, D, F 로 구분하여 출력해주는 클래스.
// 성적이 속하는 구간에 따라
// 100 >= "A" >= cutA > "B" >= cutB > "C" >= cutC > "D" >= cutD > "F".

SubjectGrade::SubjectGrade(const string& name, int credit,
               int cutA, int cutB, int cutC, int cutD) : Subject(name, credit), cutA_(cutA), cutB_(cutB), cutC_(cutC), cutD_(cutD) {}

SubjectGrade::~SubjectGrade(){}

string SubjectGrade::GetGrade(int score) const{
  if(score <= 100 && score >= cutA_)  return "A";
  if(score < cutA_ && score >= cutB_)  return "B";
  if(score < cutB_ && score >= cutC_)  return "C";
  if(score < cutC_ && score >= cutD_)  return "D";
  if(score < cutD_)  return "F";	
}