#include <vector>
#include <iostream>

#ifndef __hw09__poly_diff__
#define __hw09__poly_diff__

using namespace std;

class Polynomial{
  public:
    Polynomial(){}
    Polynomial(string Expression);
    int Differentiation(const int& value);
  private:
    vector<int> Coefficent_;
    vector<int> Difference_;
};

#endif