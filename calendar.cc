#include <iostream>
#include <string>
#include <stdlib.h>
#include "calendar.h"

using namespace std;

Date::Date() : year_(0), month_(1), day_(1) {}
Date::Date(int year, int month, int day) : year_(year), month_(month), day_(day) {}

void Date::NextDay(int n){
	day_ += n;
	while (!this->Vaild_Date(year_, month_, day_)) {
		if (day_ > 31 && (month_ == 1 || month_ == 3 || month_ == 5 || month_ == 7 || month_ == 8 || month_ == 10 || month_ == 12)) {
			++month_;
			day_ -= 31;
		}
		else if (day_ > 30 && (month_ == 4 || month_ == 6 || month_ == 9 || month_ == 11)) {
			++month_;
			day_ -= 30;
		}
		else if (day_ > 29 && Date::GetDaysInYear(year_) == 366) {
			++month_;
			day_ -= 29;
		}
		else if (day_ > 28 && Date::GetDaysInYear(year_) == 365) {
			++month_;
			day_ -= 28;
		}
		else if (day_ < 1 && (month_ == 2 || month_ == 4 || month_ == 6 || month_ == 8 || month_ == 9 || month_ == 11 || month_ == 1)) {
			--month_;
			day_ += 31;
		}
		else if (day_ < 1 && (month_ == 5 || month_ == 7 || month_ == 10 || month_ == 12)) {
			--month_;
			day_ += 30;
		}
		else if (day_ < 1 && Date::GetDaysInYear(year_) == 366) {
			--month_;
			day_ += 29;		
		}
		else if (day_ < 1 && Date::GetDaysInYear(year_) == 365) {
			--month_;
			day_ += 28;		
		}
		if (month_ > 12) {
			++year_;
			month_ -= 12;
		}
		else if (month_ < 1) {
			--year_;
			month_ += 12 ;
		}
	}
}

bool Date::Vaild_Date(int year, int month, int day) const {
	if (month < 1 || month > 12 || day < 1 || day > 31)	return false;
	if ((month == 2 || month == 4 || month == 6 || month == 9 || month == 11) && (day > 30 || day < 1))	return false;
	else if ((month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month ==12) && (day > 31 || day < 1))	return false;
	if (month == 2){
		if (Date::GetDaysInYear(year) == 366 && (day > 29 || day < 1))	return false;
		else if (Date::GetDaysInYear(year) == 365 && (day > 28 || day < 1))	return false;
	}
	return true;
}

bool Date::SetDate(int year, int month, int day){
	if (this->Vaild_Date(year, month, day)) {
		year_ = year;
		month_ = month;
		day_ = day;
		return true;
	}
	return false;
}

int Date::GetDaysInYear(int year) {
	if (year % 4 == 0){
		if (year % 100 == 0){
			if (year % 400 == 0)	return 366;
			else return 365;
		}
		else return 366;
	}
	else return 365;
}

ostream& operator<<(ostream& os, const Date& c) {
	os << c.year() << "." << c.month() << "." << c.day();
	return os;
}

istream& operator>>(istream& is, Date& c) {
	string input;
	is >> input;
	InvalidDateException temp(input);
	int year = atoi(input.substr(0, input.find('.')).c_str());
	input.erase(0, input.find('.') + 1);
	int month = atoi(input.substr(0, input.find('.')).c_str());
	input.erase(0, input.find('.') + 1);
	int day = atoi(input.c_str());
	if(!c.SetDate(year, month, day))	throw temp;
	return is;
}