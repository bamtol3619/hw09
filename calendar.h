#include <iostream>

#ifndef __hw09__calendar__
#define __hw09__calendar__

using namespace std;

class Date {
 public:
  Date();
  Date(int year, int month, int day);

  void NextDay(int n);
  bool SetDate(int year, int month, int day);
  bool Vaild_Date(int year, int month, int day) const;

  int year() const { return year_; }
  int month() const { return month_; }
  int day() const { return day_; }

 private:
  // 윤년을 판단하여 주어진 연도에 해당하는 날짜 수(365 또는 366)를 리턴.
  static int GetDaysInYear(int year);

  int year_, month_, day_;
};

struct InvalidDateException {
  string input_date;
  InvalidDateException(const string& str) : input_date(str) {}
};

// yyyy.mm.dd 형식으로 입출력.
// 사용자 입력 오류시 >> operator는 InvalidDateException을 발생할 수 있음.
ostream& operator<<(ostream& os, const Date& c);
istream& operator>>(istream& is, Date& c);

#endif