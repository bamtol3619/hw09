#include <iostream>
#include <string>
#include <stdlib.h>
#include <map>
#include <fstream>
#include "world_clock.h"

using namespace std;

WorldClock::WorldClock() : hour_(0), minute_(0), second_(0), time_difference_(0) {
	LoadTimezoneFromFile("timezone.txt");
}
WorldClock::WorldClock(int hour, int minute, int second) : hour_(hour), minute_(minute), second_(second), time_difference_(0) {}

WorldClock::~WorldClock() {
	SaveTimezoneToFile("timezone.txt");
}

map<string, int> WorldClock::timezone_;

void WorldClock::Tick(int seconds) {
	bool is_second_plus = seconds > 0 ? true : false;
	hour_ += seconds / 3600;
	seconds %= 3600;
	minute_ += seconds / 60;
	seconds %= 60;
	second_ += seconds % 60;
	if (is_second_plus){
		while (second_ >= 60) {
			second_ -= 60;
			++minute_;
		}
		while (minute_ >= 60) {
			minute_ -= 60;
			++hour_;
		}
		while (hour_ >= 24) {
			hour_ -= 24;
		}
	}
	else {
		while (second_ < 0) {
			second_ += 60;
			--minute_;
		}
		while (minute_ < 0) {
			minute_ += 60;
			--hour_;
		}
		while (hour_ < 0) {
			hour_ += 24;
		}
	}
}

bool WorldClock::SetTime(int hour, int minute, int second) {
	if (hour >= 24 || hour < 0 || minute >= 60 || minute < 0 || second >= 60 || second < 0)	return false;
	hour_ = hour;
	minute_ = minute;
	second_ = second;
	return true;
}

bool WorldClock::LoadTimezoneFromFile(const string& file_path) {
	WorldClock::timezone_["GMT"] = 0;
	WorldClock::timezone_["Seoul"] = 19;
}
void WorldClock::SaveTimezoneToFile(const string& file_path) {
	ofstream outfile;
	outfile.open("timezone.txt", ofstream::out);
	for (map<string, int>::iterator it = WorldClock::timezone_.begin(); it != WorldClock::timezone_.end(); ++it) {
		outfile << it->first << " " << it->second << endl;
	}
	outfile.close();
}

void WorldClock::AddTimezoneInfo(const string& city, int diff) {
	WorldClock::timezone_[city] = diff;
}

// 잘못된 값 입력시 false 리턴하고 원래 시간은 바뀌지 않음.
bool WorldClock::SetTimezone(const string& timezone) {
	for (map<string, int>::iterator it = WorldClock::timezone_.begin(); it != WorldClock::timezone_.end(); ++it) {
		if (it->first == timezone){
			time_difference_ = it->second;
			return true;
		}
	}
	return false;
}

// hh:mm:ss 형식으로 입출력. 표준시가 아닌 경우 (+xx)/(-xx) 형식으로 시차를 표시.
ostream& operator<<(ostream& os, const WorldClock& c) {
	if (c.time_difference() > 0) {
		if (c.hour() + c.time_difference() >= 24)	os << c.hour() + c.time_difference() - 24;
		else	os << c.hour() + c.time_difference();
		os << ":" << c.minute() << ":" << c.second() << " (+" << c.time_difference() << ")";
	}
	else if (c.time_difference() < 0) {
		if(c.hour() + c.time_difference() < 0)	os << c.hour() + c.time_difference() + 24;
		else	os << c.hour() + c.time_difference();
		os << ":" << c.minute() << ":" << c.second() << " (-" << c.time_difference() << ")";
	}
	else os << c.hour() <<":" << c.minute() << ":" << c.second();
	return os;
}

istream& operator>>(istream& is, WorldClock& c) {
	string input;
	is >> input;
	InvalidTimeException temp(input);
	int hour = atoi(input.substr(0, input.find(':')).c_str());
	input.erase(0, input.find(':') + 1);
	int minute = atoi(input.substr(0, input.find(':')).c_str());
	input.erase(0, input.find(':') + 1);
	int second = atoi(input.c_str());
	if(!c.SetTime(hour, minute, second))	throw temp;
	return is;
}